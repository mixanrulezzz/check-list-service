<?php

use App\Http\Controllers\Admin\MainController as MainAdminController;
use Illuminate\Support\Facades\Route;

Route::get('', ['as' => 'admin.dashboard', 'uses' => MainAdminController::class . '@dashboard']);
