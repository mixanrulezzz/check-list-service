<?php


namespace App\Helpers\Api;


use App\Http\Controllers\CheckListController as CheckListControllerWeb;
use App\Models\CheckListItem;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Models\CheckList as CheckListModel;

class CheckList
{
    /**
     * Получить чек-лист
     * @param User $user
     * @param int $checkListId
     * @return CheckListModel
     * @throws HttpException
     */
    public static function getCheckListForUser(User $user, int $checkListId): CheckListModel
    {
        $checkList = CheckListModel::where('id', $checkListId)->first(['id', 'creator_id']);

        if (!$checkList) {
            throw new HttpException(404);
        }

        if ($checkList->creator_id != $user->id) {
            throw new HttpException(403);
        }

        return $checkList;
    }

    /**
     * Получить пункт чек-листа
     * @param User $user
     * @param int $checkListId
     * @param int $itemId
     * @return CheckListItem
     * @throws HttpException
     */
    public static function getCheckListItemForUser(User $user, int $checkListId, int $itemId): CheckListItem
    {
        $item = CheckListItem::where('check_list_items.id', $itemId)
            ->join(
                'check_lists', 'check_list_items.check_list_id', '=', 'check_lists.id'
            )
        ->first([
            'id' => 'check_list_items.id',
            'check_list_id' => 'check_list_items.check_list_id',
            'creator_id' => 'check_lists.creator_id'
        ]);

        if (!$item) {
            throw new HttpException(404);
        }

        if ($item->check_list_id != $checkListId) {
            throw new HttpException(400);
        }

        if ($item->creator_id != $user->id) {
            throw new HttpException(403);
        }

        return $item;
    }

    /**
     * Сбросить кеш для списка чек-листов
     * @param User $user
     */
    public static function resetCheckListCacheForUser(User $user)
    {
        Cache::pull(md5(CheckListControllerWeb::CHECK_LIST_WEB_CACHE_PREFIX_KEY . $user->id));
    }
}
