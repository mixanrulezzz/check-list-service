<?php


namespace App\Http\Admin\Templates;


use SleepingOwl\Admin\Templates\TemplateDefault as SleepingOwlTemplateDefault;

class TemplateDefault extends SleepingOwlTemplateDefault
{
    /**
     * @return string
     */
    public function getMenuTop()
    {
        return __(config('sleeping_owl.menu_top'));
    }
}
