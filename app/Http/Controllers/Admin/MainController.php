<?php


namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View;
use SleepingOwl\Admin\Facades\Admin as AdminSection;


class MainController extends BaseController
{
    public function dashboard(): View
    {
        $content = __('Main information');
        return AdminSection::view($content, __('Main'));
    }
}
