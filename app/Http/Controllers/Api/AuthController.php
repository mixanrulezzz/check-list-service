<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AuthRequest;
use App\Http\Requests\Api\RegistrationRequest;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Авторизация
     * @param AuthRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     */
    public function auth(AuthRequest $request)
    {
        try {
            $request->authenticate();
            $token = explode('|', \Auth::user()->createToken('api')->plainTextToken)[1];

            return response(['token' => $token]);
        } catch (ValidationException $exception) {
            return response(['errors' => $exception->errors()], 400);
        } catch (\Exception $exception) {
            return response(['errors' => $exception->getMessage()], 501);
        }
    }

    /**
     * Регистрация
     * @param RegistrationRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     */
    public function registration(RegistrationRequest $request)
    {
        try {
            $request->registration();
            $token = explode('|', \Auth::user()->createToken('api')->plainTextToken)[1];

            return response(['token' => $token]);
        } catch (ValidationException $exception) {
            return response(['errors' => $exception->errors()], 400);
        } catch (\Exception $exception) {
            return response(['errors' => $exception->getMessage()], 501);
        }
    }
}
