<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Api\CheckList as CheckListHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PaginationRequest;
use App\Models\CheckList;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckListController extends Controller
{
    /**
     * Получить список чек-листов пользователя
     * @param PaginationRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     */
    public function get(PaginationRequest $request)
    {
        $page = $request->get('page', 0);
        $pageSize = $request->get('size', 0);

        /** @var CheckList $checkListsQueryBuilder */
        $checkListsQueryBuilder = $request->user()->checkLists()->select(['id', 'name']);

        if (!empty($pageSize) && !empty($page)) {
            $paginate = $checkListsQueryBuilder->paginate($pageSize);
            $checkLists = $paginate->items();
            $total = $paginate->total();
        } else {
            $checkLists = $checkListsQueryBuilder->get() ?? [];
            $total = $checkListsQueryBuilder->count();
        }

        return response(['check-lists' => $checkLists, 'total' => $total]);
    }

    /**
     * Создать чек-лист
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $userCheckListCount = $request->user()->checkLists()->count();
        $maxCheckListCount = $request->user()->getCheckListMaxCount();

        if ($userCheckListCount >= $maxCheckListCount) {
            throw new HttpException(403);
        }

        $newCheckList = new CheckList();
        $newCheckList->creator_id = $request->user()->id;
        $newCheckList->name = $request->get('name') ?? '';
        $newCheckList->save();
        CheckListHelper::resetCheckListCacheForUser($request->user());

        return response()->noContent(200);
    }

    /**
     * Обновить чек-лист
     * @param int $checkListId
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(int $checkListId, Request $request)
    {
        $checkList = CheckListHelper::getCheckListForUser($request->user(), $checkListId);

        if (!empty($request->get('name'))) {
            $checkList->name = $request->get('name');
        }

        $checkList->save();
        CheckListHelper::resetCheckListCacheForUser($request->user());

        return response()->noContent(200);
    }

    /**
     * Удалить чек-лист
     * @param int $checkListId
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(int $checkListId, Request $request)
    {
        $checkList = CheckListHelper::getCheckListForUser($request->user(), $checkListId);

        $checkList->delete();
        CheckListHelper::resetCheckListCacheForUser($request->user());

        return response()->noContent(200);
    }
}
