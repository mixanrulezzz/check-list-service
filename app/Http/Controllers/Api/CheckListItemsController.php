<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Api\CheckList as CheckListHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PaginationRequest;
use App\Models\CheckListItem;
use Illuminate\Http\Request;

class CheckListItemsController extends Controller
{
    /**
     * Получить список пунктов в чек-листе
     * @param int $checkListId
     * @param PaginationRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     */
    public function get(int $checkListId, PaginationRequest $request)
    {
        $page = (int)$request->get('page', 0);
        $pageSize = (int)$request->get('size', 0);

        $checkList = CheckListHelper::getCheckListForUser($request->user(), $checkListId);

        $queryBuilder = $checkList->items()->select(['id', 'value', 'is_checked']);
        if (!empty($pageSize) && !empty($page)) {
            $paginate = $queryBuilder->paginate($pageSize);
            $items = $paginate->items();
            $total = $paginate->total();
        } else {
            $items = $queryBuilder->get() ?? [];
            $total = $queryBuilder->count();
        }

        return response(['items' => $items, 'total' => $total]);
    }

    /**
     * Создать пункт чек-листа
     * @param int $checkListId
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(int $checkListId, Request $request)
    {
        $checkList = CheckListHelper::getCheckListForUser($request->user(), $checkListId);

        $newItem = new CheckListItem();
        $newItem->check_list_id = $checkList->id;
        $newItem->value = $request->get('value');
        $newItem->save();
        CheckListHelper::resetCheckListCacheForUser($request->user());

        return response()->noContent(200);
    }

    /**
     * Обновить пункт чек-листа
     * @param int $checkListId
     * @param int $itemId
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(int $checkListId, int $itemId, Request $request)
    {
        $item = CheckListHelper::getCheckListItemForUser($request->user(), $checkListId, $itemId);

        $item->value = $request->get('value');
        $item->save();
        CheckListHelper::resetCheckListCacheForUser($request->user());

        return response()->noContent(200);
    }

    /**
     * Удалить пункт чек-листа
     * @param int $checkListId
     * @param int $itemId
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(int $checkListId, int $itemId, Request $request)
    {
        $item = CheckListHelper::getCheckListItemForUser($request->user(), $checkListId, $itemId);

        $item->delete();
        CheckListHelper::resetCheckListCacheForUser($request->user());

        return response()->noContent(200);
    }

    /**
     * Отметить пункт чек-листа
     * @param int $checkListId
     * @param int $itemId
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function check(int $checkListId, int $itemId, Request $request)
    {
        $item = CheckListHelper::getCheckListItemForUser($request->user(), $checkListId, $itemId);

        $item->is_checked = true;
        $item->save();
        CheckListHelper::resetCheckListCacheForUser($request->user());

        return response()->noContent(200);
    }

    /**
     * Удалить отметку у пункта чек-листа
     * @param int $checkListId
     * @param int $itemId
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function uncheck(int $checkListId, int $itemId, Request $request)
    {
        $item = CheckListHelper::getCheckListItemForUser($request->user(), $checkListId, $itemId);

        $item->is_checked = false;
        $item->save();
        CheckListHelper::resetCheckListCacheForUser($request->user());

        return response()->noContent(200);
    }
}
