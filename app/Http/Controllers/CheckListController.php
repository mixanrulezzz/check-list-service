<?php

namespace App\Http\Controllers;

use App\Models\CheckListItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CheckListController extends Controller
{
    const CHECK_LIST_WEB_CACHE_PREFIX_KEY = 'checkLists_web_';

    /**
     * Получить все чек-листы пользователя
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function getAll()
    {
        $checkLists = Cache::remember(md5(self::CHECK_LIST_WEB_CACHE_PREFIX_KEY . \Auth::user()->id), 3600, function () {
            $checkLists = array_column(\Auth::user()->checkLists()->get()->toArray(), null, 'id');

            $checkListsItems = CheckListItem::where('check_list_id', array_keys($checkLists))
                ->get()
                ->toArray();

            foreach ($checkListsItems as $checkListsItem) {
                $checkLists[$checkListsItem['check_list_id']]['items'][] = $checkListsItem;
            }

            return $checkLists;
        });

        return view('check-list.main', ['checkLists' => $checkLists]);
    }
}
