<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Auth\LoginRequest;
use App\Traits\ApiRequestValidationTrait;

class AuthRequest extends LoginRequest
{
    use ApiRequestValidationTrait;
}
