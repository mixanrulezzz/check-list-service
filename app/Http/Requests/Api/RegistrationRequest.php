<?php

namespace App\Http\Requests\Api;

use App\Models\Role;
use App\Models\User;
use App\Traits\ApiRequestValidationTrait;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class RegistrationRequest extends FormRequest
{
    use ApiRequestValidationTrait;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'string'],
        ];
    }

    public function registration()
    {
        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => Hash::make($this->password),
        ]);

        event(new Registered($user));
        Role::where('code', Role::REGISTERED_USER_ROLE_CODE)
            ->first(['id'])
            ->users()
            ->sync($user);

        if (! Auth::attempt(array_merge($this->only('email', 'password'), ['active' => true]))) {
            throw ValidationException::withMessages([
                'email' => trans('Registration error.'),
            ]);
        }
    }
}
