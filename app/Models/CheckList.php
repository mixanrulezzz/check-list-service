<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CheckList
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $creator_id
 * @property-read \App\Models\User $creator
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\CheckListItem> $items
 * @property-read int|null $items_count
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|CheckList whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CheckList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CheckList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CheckList query()
 * @method static \Illuminate\Database\Eloquent\Builder|CheckList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CheckList whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CheckList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CheckList whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CheckList extends Model
{
    use HasFactory;

    /**
     * Получить список строк в чек-листе
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(CheckListItem::class, 'check_list_id');
    }

    /**
     * Получить создателя чек-листа
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }
}
