<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CheckListItem
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $check_list_id
 * @property string|null $value
 * @property int $is_checked
 * @property-read \App\Models\CheckList $checkList
 * @method static \Illuminate\Database\Eloquent\Builder|CheckListItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CheckListItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CheckListItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|CheckListItem whereCheckListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CheckListItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CheckListItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CheckListItem whereIsChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CheckListItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CheckListItem whereValue($value)
 * @mixin \Eloquent
 */
class CheckListItem extends Model
{
    use HasFactory;

    /**
     * Получить чек лист, к которому принадлежит строка
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function checkList()
    {
        return $this->belongsTo(CheckList::class);
    }
}
