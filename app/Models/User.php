<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Http\Middleware\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Expression;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection<int, \Illuminate\Notifications\DatabaseNotification> $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Role> $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Laravel\Sanctum\PersonalAccessToken> $tokens
 * @property-read int|null $tokens_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\CheckList> $checkLists
 * @property-read int|null $check_lists_count
 * @method static \Database\Factories\UserFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Роли, которые есть у пользователя
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role_links');
    }

    /**
     * Есть ли у пользователя доступ в админку
     * @return bool
     */
    public function hasAdminSectionAccess(): bool
    {
        $roleWithAdminSectionAccess = $this->roles()
            ->join(
                'role_permission_links',
                'roles.id', '=', 'role_permission_links.role_id'
            )->join(
                'permissions',
                'permissions.id', '=', 'role_permission_links.permission_id'
            )
            ->where('permissions.code', Permission::ADMIN_PERMISSION_CODE)
            ->where('role_permission_links.value', '1')
            ->first();

        return !empty($roleWithAdminSectionAccess);
    }

    /**
     * Список чек листов пользователя
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checkLists()
    {
        return $this->hasMany(CheckList::class, 'creator_id');
    }

    /**
     * Получить максимальное количество чек-листов, которое может иметь пользователь
     * @return int
     */
    public function getCheckListMaxCount(): int
    {
        $checkListMaxCount = $this->roles()
            ->join(
                'role_permission_links',
                'roles.id', '=', 'role_permission_links.role_id'
            )->join(
                'permissions',
                'permissions.id', '=', 'role_permission_links.permission_id'
            )
            ->where('permissions.code', Permission::CHECK_LIST_COUNT_PERMISSION_CODE)
            ->max(new Expression('CAST(`role_permission_links`.`value` AS UNSIGNED)'));

        return (int)$checkListMaxCount ?? 0;
    }
}
