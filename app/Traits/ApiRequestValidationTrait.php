<?php


namespace App\Traits;


use Illuminate\Contracts\Validation\Validator;

trait ApiRequestValidationTrait
{
    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        response($validator->errors(), 400)->send();
        die();
    }
}
