<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Artisan::call('db:seed', [
            '--class' => 'PermissionSeeder',
        ]);

        Artisan::call('db:seed', [
            '--class' => 'RoleSeeder',
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        foreach (Role::get() as $role) {
            $role->delete();
        }
        foreach (Permission::get() as $permission) {
            $permission->delete();
        }
    }
};
