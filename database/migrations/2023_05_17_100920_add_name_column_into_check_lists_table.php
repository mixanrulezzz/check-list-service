<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('check_lists', function (Blueprint $table) {
            $table->string('name')->after('id')->default('');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasColumn('check_lists', 'name')) {
            Schema::dropColumns('check_lists', 'name');
        }
    }
};
