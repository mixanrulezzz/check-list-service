<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Permission::factory()->create([
            'name'  => 'Доступ в административный раздел',
            'code'  => 'admin',
            'group' => 'admin',
            'type'  => 'bool',
        ]);

        Permission::factory()->create([
            'name'  => 'Доступ к просмотру пользователей',
            'code'  => 'read_users',
            'group' => 'users',
            'type'  => 'bool',
        ]);

        Permission::factory()->create([
            'name'  => 'Доступ к добавлению пользователей',
            'code'  => 'add_users',
            'group' => 'users',
            'type'  => 'bool',
        ]);

        Permission::factory()->create([
            'name'  => 'Доступ к изменению пользователей',
            'code'  => 'edit_users',
            'group' => 'users',
            'type'  => 'bool',
        ]);

        Permission::factory()->create([
            'name'  => 'Доступ к удалению пользователей',
            'code'  => 'delete_users',
            'group' => 'users',
            'type'  => 'bool',
        ]);

        Permission::factory()->create([
            'name'  => 'Доступ к просмотру ролей',
            'code'  => 'read_roles',
            'group' => 'roles',
            'type'  => 'bool',
        ]);

        Permission::factory()->create([
            'name'  => 'Доступ к добавлению ролей',
            'code'  => 'add_roles',
            'group' => 'roles',
            'type'  => 'bool',
        ]);

        Permission::factory()->create([
            'name'  => 'Доступ к изменению ролей',
            'code'  => 'edit_roles',
            'group' => 'roles',
            'type'  => 'bool',
        ]);

        Permission::factory()->create([
            'name'  => 'Доступ к удалению ролей',
            'code'  => 'delete_roles',
            'group' => 'roles',
            'type'  => 'bool',
        ]);

        Permission::factory()->create([
            'name'  => 'Максимальное количество чек листов',
            'code'  => 'check_list_count',
            'group' => 'check_lists',
            'type'  => 'int',
        ]);
    }
}
