<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $adminRole = Role::factory()->create([
            'name' => 'Администратор',
            'code' => 'admin'
        ]);


        $registeredUser = Role::factory()->create([
            'name' => 'Зарегистрированный пользователь',
            'code' => 'registered_user'
        ]);


        $unregisteredUser = Role::factory()->create([
            'name' => 'Незарегистрированный пользователь',
            'code' => 'unregistered_user'
        ]);

        $permissions = Permission::get();

        foreach ($permissions as $permission) {
            // Проставим админу все разрешения
            switch ($permission->code) {
                case 'admin':
                case 'read_users':
                case 'add_users':
                case 'edit_users':
                case 'delete_users':
                case 'read_roles':
                case 'add_roles':
                case 'edit_roles':
                case 'delete_roles':
                    $permission->roles()->newPivot(['role_id' => $adminRole->id, 'permission_id' => $permission->id, 'value' => '1'])->save();
                    break;
                case 'check_list_count':
                    $permission->roles()->newPivot(['role_id' => $adminRole->id, 'permission_id' => $permission->id, 'value' => '10000'])->save();
                    break;
            }

            // Проставим разрешения для зарегистрированных и незарегистрированных пользователей
            switch ($permission->code) {
                case 'read_users':
                    $permission->roles()->newPivot(['role_id' => $registeredUser->id, 'permission_id' => $permission->id, 'value' => '1'])->save();
                    $permission->roles()->newPivot(['role_id' => $unregisteredUser->id, 'permission_id' => $permission->id, 'value' => '0'])->save();
                    break;
                case 'add_users':
                    $permission->roles()->newPivot(['role_id' => $registeredUser->id, 'permission_id' => $permission->id, 'value' => '0'])->save();
                    $permission->roles()->newPivot(['role_id' => $unregisteredUser->id, 'permission_id' => $permission->id, 'value' => '1'])->save();
                    break;
                case 'admin':
                case 'edit_users':
                case 'delete_users':
                case 'read_roles':
                case 'add_roles':
                case 'edit_roles':
                case 'delete_roles':
                    $permission->roles()->newPivot(['role_id' => $registeredUser->id, 'permission_id' => $permission->id, 'value' => '0'])->save();
                    $permission->roles()->newPivot(['role_id' => $unregisteredUser->id, 'permission_id' => $permission->id, 'value' => '0'])->save();
                    break;
                case 'check_list_count':
                    $permission->roles()->newPivot(['role_id' => $registeredUser->id, 'permission_id' => $permission->id, 'value' => '100'])->save();
                    $permission->roles()->newPivot(['role_id' => $unregisteredUser->id, 'permission_id' => $permission->id, 'value' => '0'])->save();
                    break;
            }
        }
    }
}
