<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Check-lists') }}
        </h2>
    </x-slot>

    <div class="grid mx-auto grid-cols-1 gap-x-8 gap-y-8 mt-5 sm:px-2 md:grid-cols-2 lg:mx-0 lg:max-w-none lg:grid-cols-4 lg:px-6">
        @foreach($checkLists as $checkList)
            <div class="bg-yellow-100 flex-col shadow-sm sm:rounded-lg">
                <div class="overflow-hidden">
                    <div class="p-4 text-black-50">
                        <p class="font-bold">{{ $checkList['name'] }}</p>
                    </div>
                    @if(!empty($checkList['items']))
                        <div class="p-4 pt-0 text-black-100">
                            @foreach($checkList['items'] as $item)
                                <div class="flex gap-x-2">
                                    <input class="rounded-md my-auto" type="checkbox" disabled @checked($item['is_checked'])>
                                    <p>{{ $item['value'] }}</p>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</x-app-layout>
