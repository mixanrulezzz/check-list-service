<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CheckListController;
use App\Http\Controllers\Api\CheckListItemsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/auth', [AuthController::class, 'auth']);
Route::post('/registration', [AuthController::class, 'registration']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/check-lists', [CheckListController::class, 'get']);
    Route::post('/check-lists', [CheckListController::class, 'create']);
    Route::patch('/check-lists/{checkListId}', [CheckListController::class, 'update']);
    Route::delete('/check-lists/{checkListId}', [CheckListController::class, 'delete'])->whereNumber('checkListId');

    Route::get('/check-lists/{checkListId}/items', [CheckListItemsController::class, 'get'])->whereNumber('checkListId');
    Route::post('/check-lists/{checkListId}/items', [CheckListItemsController::class, 'create'])->whereNumber('checkListId');
    Route::patch('/check-lists/{checkListId}/items/{itemId}', [CheckListItemsController::class, 'update'])->whereNumber(['checkListId', 'itemId']);
    Route::delete('/check-lists/{checkListId}/items/{itemId}', [CheckListItemsController::class, 'delete'])->whereNumber(['checkListId', 'itemId']);

    Route::patch('/check-lists/{checkListId}/items/{itemId}/check', [CheckListItemsController::class, 'check'])->whereNumber(['checkListId', 'itemId']);
    Route::patch('/check-lists/{checkListId}/items/{itemId}/uncheck', [CheckListItemsController::class, 'uncheck'])->whereNumber(['checkListId', 'itemId']);
});
